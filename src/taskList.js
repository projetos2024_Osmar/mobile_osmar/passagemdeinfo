import React from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";

const TaskList = ({navigation})=>{
    const tasks = [{
        id: 1,
        title: 'Ir ao mercado',
        date: '2024-02-27',
        time: '10:00',
        adress: "Super Mercado",
    },
    {
        id: 2,
        title: 'Ir à academia',
        date: '2024-02-28',
        time: '08:30',
        adress: "Academia Fitness",
    },
    {
        id: 3,
        title: 'Reunião de trabalho',
        date: '2024-03-01',
        time: '14:00',
        adress: "Escritório Central",
    }
];

    const taskPress=(task)=>{
        navigation.navigate('DetalhesDasTarefas', {task});
    };

    return(
        <View>
            <FlatList 
            data={tasks}
            keyExtractor={(item)=> item.id.toString}
            renderItem={({item})=>(
                <TouchableOpacity style={styles.container} onPress={()=> taskPress(item)}>
                    <Text style={styles.texto}>{item.title}</Text>
                </TouchableOpacity>
            )}/>
        </View>
    );
}

const styles = StyleSheet.create({
    texto: {
        fontSize: 26,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        borderWidth: 0.2,
        backgroundColor: 'lightgrey',
        marginBottom: 10,
    }

});


export default TaskList;