import React from "react";
import { View, Text, TouchableOpacity, FlatList, StyleSheet } from "react-native";

const TaskDetails = ({route})=>{
    const {task} = route.params;

    return(
        <View style={styles.container}>
            <Text style={styles.texto}>Data: {task.date}</Text>
            <Text style={styles.texto}>Horário: {task.time}</Text>
            <Text style={styles.texto}>Endereço: {task.adress}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    texto: {
        fontSize: 20,
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'flex-start',
        gap: 8,
    }

});

export default TaskDetails;